// stdlib functionality
#include <iostream>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLorentzVector.h>
#include <TCanvas.h>

// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/EgammaContainer.h"
#include "xAODBTagging/BTaggingContainer.h"

// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"

// jet calibration
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"


using namespace std;

int main(int argc, char** argv) {
  // initialize the xAOD EDM
  xAOD::Init();


  // setup the jet calibration tool

  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;

  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");

  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  );
  JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
  JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              );
  JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       );
  JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            );

  JetCalibrationTool_handle.retrieve();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/data/DAOD_EXOT27.17882744._000026.pool.root.1";
  if (argc >= 2) inputFilePath = argv[1];
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // get the number of events in the file to loop over
  Long64_t numEntries = -1;
  if(argc >= 4) numEntries = std::atoi(argv[3]);
  if(numEntries == -1) numEntries = event.getEntries();

  // for counting events
  unsigned count = 0;


  cout << "N entries: " << numEntries << endl;
  
  // initialize histograms
  TH1D* h_nJets     = new TH1D("h_nJets", "Number of Jets", 20, 0, 20);
  TH1D* h_nJetCalib = new TH1D("h_nJetCalib", "Number of Jets after calibration", 20, 0, 20);
  TH1D* h_nMuon     = new TH1D("h_nMoun", "Number of Muons", 10, 0, 10);
  TH1D* h_nEGamma   = new TH1D("h_nEGamma", "Number of Electrons", 10, 0, 10);
  TH1D* h_mjj       = new TH1D("h_mjj",   "Dijet Invariant Mass", 20, 0, 500);
  TH1D* h_mjj_calib = new TH1D("J_mjj_calib", "Calibrated Dijet Invariant Mass", 20, 0, 500);
  TH1D* h_muu       = new TH1D("h_muu",   "DiMuon Invariant Mass", 100, 0, 250);
  TH2D* h_muu_mjj   = new TH2D("h_muu_mjj", "DiJet vs DiMuon invariant mass", 100, 0, 250, 100, 0, 500);

  double mjj, muu;
  Bool_t hasMjj, hasMuu;

  JetSelectionHelper jet_selector;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    hasMjj = kFALSE;
    hasMuu = kFALSE;

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    //std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;
    double MCweight = ei->mcEventWeights()[0];

    
    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    const xAOD::EgammaContainer* electrons = nullptr;
    //event.retrieve(electrons, "");

    const xAOD::MuonContainer* muons = nullptr;
    event.retrieve(muons, "Muons");


    h_nMuon->Fill(muons->size());
    //h_nEGamma->Fill(electrons->size());



    std::vector<xAOD::Jet> jets_raw;
    std::vector<xAOD::Jet> jets_calib;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event


      xAOD::Jet *calibratedjet;
      JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);
      if (jet_selector.isJetGood(calibratedjet) && jet_selector.isJetBFlavor(calibratedjet)) {
	jets_calib.push_back(*calibratedjet);
      }
      if (jet_selector.isJetGood(jet)) {
	jets_raw.push_back(*jet);
      }
      delete calibratedjet;
    }

    h_nJets->Fill(jets_raw.size());
    h_nJetCalib->Fill(jets_calib.size());
    if (jets_raw.size() > 1) {
      mjj = (jets_raw.at(0).p4()+jets_raw.at(1).p4()).M()/1000.;
      h_mjj->Fill(mjj, MCweight);
      hasMjj = kTRUE;
    }
    if (jets_calib.size() > 1) {
      mjj = (jets_calib.at(0).p4()+jets_calib.at(1).p4()).M()/1000.;
      h_mjj_calib->Fill(mjj, MCweight);
      hasMjj = kTRUE;
    }
    if (muons->size() > 1) {
      muu = (muons->at(0)->p4()+muons->at(1)->p4()).M()/1000.;
      h_muu->Fill(muu);
      hasMuu = kTRUE;
    }
    if (hasMuu && hasMjj) {
      h_muu_mjj->Fill(muu, mjj);
    }

    // counter for the number of events analyzed thus far
    count += 1;
    if (count % 1000 == 0) cout << "Processing event " << count << endl;
  }


  // write out histograms to file
  TString outputFilePath = "Analysis_hists.root";
  if (argc >= 3) outputFilePath = argv[2];
  TFile *MyFile = new TFile(outputFilePath, "RECREATE");
  
  MyFile->cd();

  h_nJets->GetXaxis()->SetTitle("N Jets Per Event");
  h_nJets->GetYaxis()->SetTitle("N Events/Bin");
  
  h_nJetCalib->GetXaxis()->SetTitle("N Calibrated Jets Per Event");
  h_nJetCalib->GetYaxis()->SetTitle("N events/Bin");

  h_nMuon->GetXaxis()->SetTitle("N Muons Per Event");
  h_nMuon->GetXaxis()->SetTitle("N Events/Bin");

  h_nEGamma->GetXaxis()->SetTitle("N Electrons Per Event");
  h_nEGamma->GetYaxis()->SetTitle("N Events/Bin");

  h_mjj->GetXaxis()->SetTitle("Dijet Invariant Mass [GeV]");
  h_mjj->GetYaxis()->SetTitle("N Events/5 Gev");

  h_mjj_calib->GetXaxis()->SetTitle("Dijet Invariant Mass [GeV]");
  h_mjj_calib->GetXaxis()->SetTitle("N Events/5 GeV");
  h_mjj_calib->SetLineColor(kRed);

  h_muu->GetXaxis()->SetTitle("DiMuon Invariant Mass [GeV]");
  h_muu->GetYaxis()->SetTitle("N Events/5 GeV");

  h_muu_mjj->GetXaxis()->SetTitle("DiMuon Invariant Mass [GeV]");
  h_muu_mjj->GetYaxis()->SetTitle("Dijet Invariant Mass [GeV]");

  h_nJets    ->Write();
  h_nMuon    ->Write();
  h_nEGamma  ->Write();
  h_mjj      ->Write();
  h_mjj_calib->Write();
  h_muu      ->Write();
  h_muu_mjj  ->Write();
  MyFile->Close();

  TCanvas* c1 = new TCanvas("c1", "My cool canvas", 400, 400);
  c1->cd();
  h_mjj->Draw();
  h_mjj_calib->Draw("same");
  c1->Print("mjj.pdf");
  

  // exit from the main function cleanly
  return 0;
}
