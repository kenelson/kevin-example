// ROOT includes
#include <TH1D.h>
#include <TString.h>
#include <TFile.h>

// C++ includes
#include <fstream>

int main(int argc, char* argv[]) {

  TString inputFilename = "Analysis_hists.root";
  if (argc >= 2) inputFilename = argv[1];

  TString outputFilename = "Analysis_hist.txt";
  if (argc >= 3) outputFilename = argv[2];

  TFile* inputRootFile = TFile::Open(inputFilename);

  TH1D* hist;
  hist = (TH1D*)inputRootFile->Get("J_mjj_calib");

  
  TString line1 = "";
  TString line2 = "";
  
  for (int bin = 1; bin < hist->GetNbinsX(); bin++) {
    line1 += TString::Format("%f ", hist->GetBinLowEdge(bin));
    line2 += TString::Format("%f ", hist->GetBinContent(bin));
  }
  line1 += TString::Format("%f", hist->GetBinLowEdge(hist->GetNbinsX()));
  line1 += "\n";


  std::ofstream ofs (outputFilename, std::ofstream::out);
  ofs << line1;
  ofs << line2;
  ofs.close();

  return 0;
}
